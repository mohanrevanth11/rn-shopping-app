import React, {Component} from 'react';
import {Provider} from 'react-redux';
import {createStore, applyMiddleware} from 'redux';
import reducers from './app/redux/reducers';
import AppNavigator from './app/navigator/AppNavigator';

class App extends Component {
  render() {
    const store = createStore(reducers, {}, applyMiddleware());
    return (      
      <Provider store = {store}>              
        <AppNavigator/>
      </Provider>
    );
  }
}

export default App;

