import React, {Component} from 'react';
import {View, Text, StyleSheet, TouchableOpacity} from 'react-native';
import {connect} from 'react-redux';
import {addToCart, removeFromCart} from '../redux/actions';
import {FontAwesome} from '@expo/vector-icons'
import AppStyle from '../styles/AppStyles'

class ProductItem extends Component{

    addItemToCart(item)
    {        
        this.props.addToCart(item);
    }

    removeItemFromCart(item)
    {        
        this.props.removeFromCart(item);
    }

    renderAddButton(item){                  
        return(
            <TouchableOpacity onPress = {() => this.addItemToCart(item)} style={styles.addButton}>
                <FontAwesome
                    name="cart-plus"
                    size={20}
                    color  = {AppStyle.APP_COLOR_SECONDARY}
                />
            </TouchableOpacity>
        );
    }    
    renderRemoveButton(item){        
        return(
            <TouchableOpacity onPress = {() => this.removeItemFromCart(item)} style={styles.removeButton}>
                <FontAwesome
                    name="times"
                    size={20}
                    color  = {AppStyle.REMOVE_PRODUCT}
                />
            </TouchableOpacity>
        );        
    }
    renderQuantity(item){        
        return(
            <Text style={styles.quantityText}>{item.quantity}</Text>
        );        
    }
    renderTotal(item){        
        let totalAmount = (item.quantity * item.price).toFixed(2);
        return(                
            <Text style={styles.subTotalText}>$ {totalAmount}</Text>
        );        
    }        
    
    render(){
        const {data, showAdd, showRemove} = this.props;
        return(
            <View style={styles.container}>
                <View style={{flex:1}}>
                    <Text style={styles.productText}>{data.name}</Text>  
                    <Text style={styles.priceText}>Price: ${data.price}</Text>
                </View>
                {showAdd ? 
                    <View>
                        {this.renderAddButton(data)}
                    </View>
                    :
                    <View style= {styles.otherTextContainer}>                                  
                        {this.renderQuantity(data)}
                        {this.renderTotal(data)}                    
                        {this.renderRemoveButton(data)}
                    </View>}
            </View>
        );
    }    
}


export default connect (null, {addToCart, removeFromCart})(ProductItem);

const styles = StyleSheet.create({
    productText: {
        fontSize: 18,
        color: '#000', 
        fontWeight: 'bold'               
    },
    otherTextContainer: {
        flexDirection: 'row', 
        flex: 1,
        alignItems: 'center'      
    },
    priceText: {                        
        fontSize: 14,        
    },
    subTotalText:{        
        fontSize: 16,
        flex:3        
    },
    container:{
        flexDirection: 'row', 
        justifyContent: 'space-between',        
        paddingVertical: 16
    },
    quantityText:{        
        fontSize: 16, 
        flex: 1       
    },
    addButton:{
        padding: 16, 
        alignItems: 'flex-end'
    },
    removeButton: {
        padding: 16
    }
});