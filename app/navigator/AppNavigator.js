import React, {Component} from 'react';
import {createStackNavigator, createAppContainer} from 'react-navigation';
import ListScreen from '../screens/ProductList';
import CartScreen from '../screens/CartScreen';

const RootNavigator = createStackNavigator(
    {
        List: ListScreen,
        Cart: CartScreen
    },{
        initialRouteName: 'List',        
    }
);

const AppContainer = createAppContainer(RootNavigator);

class AppNavigator extends Component{ 
    render(){
        return(
            <AppContainer/>
        );        
    }   
}
export default AppNavigator;