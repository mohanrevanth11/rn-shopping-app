import {ADD_TO_CART, REMOVE_FROM_CART, GET_CART_ITEMS} from '../types';

export const addToCart = (item) => ({
    type: ADD_TO_CART,
    payload: item
});

export const removeFromCart = (item) => ({
    type: REMOVE_FROM_CART,
    payload: item
});

export const getCartItems = () => ({
    type: GET_CART_ITEMS,
});