import data from '../data/Products.json';
import {PRODUCTS_LIST} from '../types';

export const getProducts = () => ({
    type: PRODUCTS_LIST,
    payload: data
});