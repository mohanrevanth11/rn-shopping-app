import {ADD_TO_CART, REMOVE_FROM_CART, GET_CART_ITEMS} from '../types';

const INITIAL_STATE = {    
    totalQuantity: 0,
    cart: [],
    totalPrice: 0
};

export const mapItem = item => ({
    name: item.name,
    price: item.price,
    id: item.id,
    quantity: 1
});

export default function (state = INITIAL_STATE, action){    
    switch(action.type){
        case ADD_TO_CART:            
            const {name, price} = action.payload;
            var itemExist = false;
            let map = state.cart.map(x => {
                if(x.name === name)
                {
                    x.quantity++;
                    itemExist = true;
                }}
            )   
            if(itemExist){
                let totalQuantity = state.totalQuantity + 1;
                let totalPrice = state.totalPrice + price;
                return {...state, totalQuantity, totalPrice}
            }else{
                let newItem = mapItem(action.payload);                 
                let cart = [newItem, ...state.cart];   
                let totalQuantity = state.totalQuantity + 1;  
                let totalPrice = state.totalPrice + price; 
                return { ...state, cart, totalQuantity, totalPrice}
            }               
        case REMOVE_FROM_CART:             
            let cart = state.cart;
            let totalPrice = state.totalPrice - (action.payload.price*action.payload.quantity);
            cart = cart.filter((item) => item.name !== action.payload.name);   
            let totalQuantity = state.totalQuantity - action.payload.quantity;        
            return { ...state, cart, totalQuantity, totalPrice}
        case GET_CART_ITEMS:
            return { ...state, data: action.payload}
        default:
            return state;

    }

}