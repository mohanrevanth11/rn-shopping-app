import {combineReducers} from 'redux';
import ProductsReducer from './productsReducer';
import CartReducer from './cartReducer';

export default combineReducers({
    products: ProductsReducer,
    cart: CartReducer
});