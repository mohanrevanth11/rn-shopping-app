import {PRODUCTS_LIST} from '../types';

const INITIAL_STATE = {
    data: [],
};

export default function (state = INITIAL_STATE, action){
    switch(action.type){
        case PRODUCTS_LIST:
            return { ...state, data: action.payload}
        default:
            return state;

    }

}