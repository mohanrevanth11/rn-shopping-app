export const PRODUCTS_LIST = "products_list";
export const ADD_TO_CART = "add_to_cart";
export const REMOVE_FROM_CART = "remove_from_cart";
export const GET_CART_ITEMS = "get_cart_items";