import React, {Component} from 'react';
import {View, Text, FlatList, TouchableOpacity, Button, StyleSheet} from 'react-native';
import {connect} from 'react-redux';
import ProductItem from '../components/ProductItem'
import AppStyles from '../styles/AppStyles'

class CartScreen extends Component{

    static navigationOptions = {
        headerTitle: 'Your Cart      ',
        headerStyle:{            
            backgroundColor: AppStyles.APP_COLOR_PRIMARY,
        },
        headerTitleStyle: {
          color: AppStyles.HEADER_TITLE_STYLE,          
        },
        headerTintColor:AppStyles.HEADER_BACK_TITLE_STYLE
    };
    renderItem(item){
        return(
            <ProductItem data={item} showRemove showQuantity showTotal/>
        );
    }
    renderListEmpty()
    {
        return(
            <View style={styles.emptyContainer}>
                <Text style = {styles.empty}>You do not have any items in your cart    </Text>
            </View>
        );        
    }
    render(){
        const {cart, totalQuantity, totalPrice} = this.props;
        var price = (totalPrice).toFixed(2);  
        var item = totalQuantity === 1? "item": "items";      
        return(
            <View style={styles.container}>                
                <FlatList
                    keyExtractor = {(item) => item.name}
                    data= {cart}
                    renderItem = {({item}) => this.renderItem(item)}
                    ListEmptyComponent = {this.renderListEmpty()}
                    contentContainerStyle={styles.emptyListTextCenter}
                    style={styles.list}
                />    
                {price>0 ? 
                    <View style= {styles.cartTotal}> 
                            <Text style={styles.price}>Cart Subtotal ({totalQuantity} {item})</Text>                                         
                            <Text style={[styles.price, styles.boldFont]}>$ {price}      </Text>                        
                    </View>
                    : null   
                }                                       
            </View>
        );
    }
}

const mapStateToProps = state => ({
    cart: state.cart.cart,
    totalQuantity: state.cart.totalQuantity,
    totalPrice: state.cart.totalPrice,

});

export default connect(mapStateToProps, null)(CartScreen);

const styles= StyleSheet.create({
    price: {
        fontSize: 20,
        alignSelf: 'center',
        paddingBottom: 8,
        color: AppStyles.APP_COLOR_PRIMARY
    },
    cartTotal:{
        elevation: 4, 
        paddingTop: 8,         
        backgroundColor: '#E8E8E8',
        shadowRadius: 2,
        shadowOffset: {
            width: 0,
            height: -3,
        },
        shadowColor: '#000000',            
    },
    empty:{
        fontSize: 16,
        fontWeight: 'bold',        
    },
    emptyContainer:{
        alignItems: 'center',
        justifyContent:'center',
        flex:1
    },
    container:{
        flex:1,        
    },
    list:{
        paddingVertical: 16, 
        paddingLeft: 16
    },
    boldFont:{
        fontWeight: 'bold',
    },
    emptyListTextCenter:{
        flexGrow: 1
    }
});
