import React, {Component} from 'react';
import {View, Text, FlatList, TouchableOpacity, StyleSheet} from 'react-native';
import {connect} from 'react-redux';
import {getProducts} from '../redux/actions'
import AppStyles from '../styles/AppStyles'
import ProductItem from '../components/ProductItem'
import {FontAwesome} from '@expo/vector-icons'

class ProductList extends Component{

    static navigationOptions = ({ navigation, state }) => ({        
        title: 'Products      ',
        headerStyle:{            
            backgroundColor: AppStyles.APP_COLOR_PRIMARY,
        },
        headerTitleStyle: {
          color: AppStyles.HEADER_TITLE_STYLE,          
        },        
        headerRight: (
            <TouchableOpacity onPress = {() => navigation.navigate('Cart')} style={styles.headerCart}>
                <FontAwesome
                name = "shopping-cart"
                size = {24}
                color = "white"
                />                 
            </TouchableOpacity>
        ),
    });
    
    componentWillMount(){        
        this.props.getProducts();        
    }

    renderItem(item){
        return(
            <ProductItem data={item} showAdd/>
        );
    }
    render(){
        const {productData, navigation} = this.props;
        return(            
            <View style={styles.container}>                   
                <FlatList
                    keyExtractor = {(item) => item.name}
                    data= {productData}
                    renderItem = {({item}) => this.renderItem(item)}
                />  
                <TouchableOpacity onPress = {() => navigation.navigate('Cart')} style={styles.bottomButton}>
                    <Text style= {styles.buttonText}> GO TO CART </Text>
                </TouchableOpacity>              
            </View>
        );
    }
}

const mapStateToProps = state => ({
    productData: state.products.data,
    totalQuantity: state.cart.totalQuantity
});

export default connect(mapStateToProps, {getProducts})(ProductList);

const styles = StyleSheet.create({
    container: {
        flex: 1,
        paddingVertical: 16,
        paddingLeft: 16
    },
    bottomButton:{
        backgroundColor: AppStyles.APP_COLOR_PRIMARY,
        alignItems:"center",
        padding: 16,
        marginRight: 16,
        borderRadius: 4
    },
    buttonText: {
        fontSize: 16, 
        fontWeight: 'bold', 
        color:'#FFF'
    },
    headerCart:{
        paddingHorizontal: 16
    }
});